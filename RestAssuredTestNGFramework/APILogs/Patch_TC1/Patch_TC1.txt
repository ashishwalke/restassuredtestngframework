End point : https://reqres.in/api/users/2

Request Body : 

{
    "name": "Shreya",
    "job": "PDO"
}

Response Body : 

{"name":"Shreya","job":"PDO","updatedAt":"2023-10-19T03:03:43.946Z"}